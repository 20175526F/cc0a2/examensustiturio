package com.example.examensustitutorio;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ResultadoActivity extends AppCompatActivity {

    ImageView iv_resultado;
    TextView tv_final;
    Button btn_jugar;
    Button btn_terminar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        iv_resultado = findViewById(R.id.iv_resultado);
        tv_final = findViewById(R.id.tv_resultadoFinal);
        btn_jugar = findViewById(R.id.btn_jugarNuevo);
        btn_terminar = findViewById(R.id.btn_terminar);

        Intent intent = getIntent();

        String text = intent.getStringExtra("TEXT");

        int puntajeLocal = intent.getIntExtra("PUNTAJE",0);
        int nroVidas = intent.getIntExtra("VIDAS",0);

        if (text.equals("Gano")){
            iv_resultado.setImageResource(R.drawable.leon);
            tv_final.setText("Gano");
        }
        if(text.equals("Perdio")){
            iv_resultado.setImageResource(R.drawable.tortuga);
            tv_final.setText("Perdio");
        }

        Intent intent_nuevo_juego = new Intent(ResultadoActivity.this, MainActivity.class);

        btn_jugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String snuevo = "Nuevo";
                intent_nuevo_juego.putExtra("TEXT2", snuevo);
                intent_nuevo_juego.putExtra("PUNTAJE",puntajeLocal);
                intent_nuevo_juego.putExtra("VIDAS",nroVidas );
                startActivity(intent_nuevo_juego);
            }
        });

        btn_terminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Resources res = getResources();
                AlertDialog.Builder builder=new AlertDialog.Builder(ResultadoActivity.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(String.format(res.getString(R.string.game_msg_lose)));
                builder.setPositiveButton("Si",(dialogInterface,i)->{
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                });
                builder.setNegativeButton("No",(dialogInterface,i)->{
                    Intent intent1=new Intent(ResultadoActivity.this,MainActivity.class);
                    startActivity(intent1);
                    finish();
                });
                builder.create().show();
            }
        });

    }
}