package com.example.examensustitutorio;

public class Score {
    private int puntuacion;
    private int nroIntentos;

    public Score(int puntuacion, int nroIntentos) {
        this.puntuacion = puntuacion;
        this.nroIntentos = nroIntentos;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public int getNroIntentos() {
        return nroIntentos;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    public void setNroIntentos(int nroIntentos) {
        this.nroIntentos = nroIntentos;
    }
    public void adivina(){
        puntuacion = puntuacion+10;
    }
    public void noAdivina(){
        if (puntuacion>5){
            puntuacion = puntuacion-5;
        }
        else{
            puntuacion = 0;
        }
        nroIntentos --;
    }

}
