package com.example.examensustitutorio;

public class Operacion {
    private String operacion;
    private int operador1;
    private int operador2;
    private int resultado;

    public Operacion(int operador1, int operador2, String operacion){
        this.operador1 = operador1;
        this.operador2 = operador2;
        this.operacion = operacion;
    }

    public int Suma(int operador1, int operador2){
        return operador1+operador2;
    }

    public int Multiplica(int operador1, int operador2){
        return operador1*operador2;
    }
    public int Resta(int operador1, int operador2){
        return operador1-operador2;
    }

    public void Opera(int operador1, int operador2, String operacion){
        if (operacion.equals("Suma")){
            resultado = operador1 + operador2;
        }
        if (operacion.equals("Multiplica")){
            resultado = operador1 * operador2;
        }
        if (operacion.equals("Resta")){
            resultado = operador1 - operador2;
        }
    }

    public int getOperador1() {
        return operador1;
    }

    public int getOperador2() {
        return operador2;
    }

    public int getResultado() {
        return resultado;
    }

    public void setOperador1(int operador1) {
        this.operador1 = operador1;
    }

    public void setOperador2(int operador2) {
        this.operador2 = operador2;
    }
}
