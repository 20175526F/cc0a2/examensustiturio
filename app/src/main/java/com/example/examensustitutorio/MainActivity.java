package com.example.examensustitutorio;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnSuma, btnMultiplica, btnResta;
    String nuevo="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSuma = findViewById(R.id.btn_suma);
        btnMultiplica = findViewById(R.id.btn_multiplica);
        btnResta = findViewById(R.id.btn_resta);


        Intent intent = new Intent(MainActivity.this, OperacionActivity.class);

        String textNuevo = intent.getStringExtra("TEXT2");

        String text = intent.getStringExtra("TEXT");
        int puntajeLocal = intent.getIntExtra("PUNTAJE",0);
        int nroVidasLocal = intent.getIntExtra("VIDAS",0);

        btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sText = "Suma";
                intent.putExtra("TEXT", sText);
                intent.putExtra("NUEVO", textNuevo);
                intent.putExtra("PUNTAJE", puntajeLocal);
                intent.putExtra("VIDAS", nroVidasLocal);
                startActivity(intent);
            }
        });

        btnMultiplica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sText = "Multiplica";
                intent.putExtra("TEXT", sText);
                intent.putExtra("NUEVO", textNuevo);
                intent.putExtra("PUNTAJE", puntajeLocal);
                intent.putExtra("VIDAS", nroVidasLocal);
                startActivity(intent);
            }
        });

        btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sText = "Resta";
                intent.putExtra("TEXT", sText);
                intent.putExtra("NUEVO", textNuevo);
                intent.putExtra("PUNTAJE", puntajeLocal);
                intent.putExtra("VIDAS", nroVidasLocal);
                startActivity(intent);
            }
        });



    }
}