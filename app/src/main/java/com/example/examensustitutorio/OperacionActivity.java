package com.example.examensustitutorio;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class OperacionActivity extends AppCompatActivity {

    TextView tv_puntaje, tv_nroIntentos, tv_operacion;
    EditText et_resultado;
    Button btn_comprobar, btn_siguiente;

    int numero1, numero2, totalPuntaje, totalVidas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operacion);

        tv_puntaje = findViewById(R.id.tv_puntaje);
        tv_nroIntentos = findViewById(R.id.tv_nro_intentos);
        tv_operacion = findViewById(R.id.tv_operacion);
        et_resultado = findViewById(R.id.et_operacion);
        btn_comprobar = findViewById(R.id.btn_comprobar);
        btn_siguiente = findViewById(R.id.btn_sgte);

        numero1 = generaRandom();
        numero2 = generaRandom();

        Score score = new Score(0,3);


        Intent intent = getIntent();
        String texta = intent.getStringExtra("TEXT");
        String nuevo = intent.getStringExtra("NUEVO");

        int puntajeLocal = intent.getIntExtra("PUNTAJE",0);
        int nroVidasLocal = intent.getIntExtra("VIDAS",0);

        if (nroVidasLocal>0){
            score.setPuntuacion(puntajeLocal);
            score.setNroIntentos(nroVidasLocal*2);
        }


        tv_puntaje.setText("Puntaje: "+ score.getPuntuacion());
        tv_nroIntentos.setText("Numero de Intentos: "+ score.getNroIntentos());

        Operacion operacion = new Operacion(numero1,numero2, texta);

        if (texta.equals("Suma")){
            tv_operacion.setText(String.valueOf(numero1)+ "+" + String.valueOf(numero2) +" = ");
        }
        if(texta.equals("Multiplica")){
            tv_operacion.setText(String.valueOf(numero1)+ "x" + String.valueOf(numero2) +" = ");
        }
        if(texta.equals("Resta")){
            tv_operacion.setText(String.valueOf(numero1)+ "-" + String.valueOf(numero2) +" = ");
        }

        Intent intent1 = new Intent(OperacionActivity.this, ResultadoActivity.class);

        btn_comprobar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operacion.Opera(numero1,numero2,texta);
                if(et_resultado.getText().toString().equals(String.valueOf(operacion.getResultado()) )){
                    score.adivina();
                    tv_puntaje.setText("Puntaje: "+ score.getPuntuacion());
                    tv_nroIntentos.setText("Numero de Intentos: "+ score.getNroIntentos());
                    if(score.getPuntuacion()==100){
                        intent1.putExtra("TEXT", "Gano");
                        intent1.putExtra("PUNTAJE", score.getPuntuacion());
                        intent1.putExtra("VIDAS", score.getNroIntentos());
                        startActivity(intent1);
                    }
                }
                else{
                    score.noAdivina();
                    tv_puntaje.setText("Puntaje: "+ score.getPuntuacion());
                    tv_nroIntentos.setText("Numero de Intentos: "+ score.getNroIntentos());
                    if(score.getNroIntentos()==0){
                        intent1.putExtra("TEXT", "Perdio");
                        intent1.putExtra("PUNTAJE", score.getPuntuacion());
                        intent1.putExtra("VIDAS", score.getNroIntentos());
                        startActivity(intent1);
                    }
                }
            }
        });

        btn_siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                numero1 = generaRandom();
                numero2 = generaRandom();

                et_resultado.setText("");

                Operacion operacion = new Operacion(numero1,numero2, texta);
                if (texta.equals("Suma")){
                    tv_operacion.setText(String.valueOf(numero1)+ "+" + String.valueOf(numero2) +" = ");
                }
                if(texta.equals("Multiplica")){
                    tv_operacion.setText(String.valueOf(numero1)+ "x" + String.valueOf(numero2) +" = ");
                }
                if(texta.equals("Resta")){
                    tv_operacion.setText(String.valueOf(numero1)+ "-" + String.valueOf(numero2) +" = ");
                }

            }
        });

    }

    public int generaRandom(){
        int randomNumber;
        Random r=new Random();
        return r.nextInt(90)+10;
    }
}